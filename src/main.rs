use std::fmt;

struct Company {
    name: String,
    employees: Vec<Employee>,
    number_of_employees: usize,
}

impl Company {
    fn new(name: &str, employees: &Vec<Employee>) -> Self {
        let number_of_employees = employees.len();
        Company {
            name: name.to_string(),
            employees: employees.to_vec(),
            number_of_employees,
        }
    }

    fn list_employees(&self) {
        for employee in &self.employees {
            println!("{employee}");
        }
    }
}

impl fmt::Display for Company {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Company {
            name, employees, ..
        } = self;

        write!(f, "Name: Company: {name}\nEmployees:")?;
        for employee in employees {
            write!(f, "\n{employee}")?;
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
enum Title {
    Manager,
    Secretary,
    Janitor,
}

#[derive(Clone)]
struct Employee {
    first_name: String,
    last_name: String,
    age: u8,
    title: Title,
}

impl fmt::Display for Employee {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Name: {} {}, Age: {}, Title: {:?}",
            self.first_name, self.last_name, self.age, self.title
        )
    }
}

fn main() {
    let john_smith = Employee {
        first_name: "John".to_string(),
        last_name: "Smith".to_string(),
        age: 39,
        title: Title::Manager,
    };
    let jane_smith = Employee {
        first_name: "Jane".to_string(),
        last_name: "Smith".to_string(),
        age: 28,
        title: Title::Secretary,
    };
    let fred_johnson = Employee {
        first_name: "Fred".to_string(),
        last_name: "Johnson".to_string(),
        age: 34,
        title: Title::Janitor,
    };

    let employees = vec![john_smith, jane_smith, fred_johnson];

    let honda = Company::new("Honda", &employees);
    let toyota = Company::new("Toyota", &employees);

    honda.list_employees();
    toyota.list_employees();
    println!();

    println!("{honda}");
}
